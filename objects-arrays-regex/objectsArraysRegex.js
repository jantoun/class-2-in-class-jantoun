/* 
 * Create an object representation of yourself
 * should include firstName, lastName, 'favorite food'
 * should also include a mom object and a dad object with
 * the same properties as above
 */
let me = {
    firstName: 'Jo-Anne Antoun',
    lastName: 'Antoun',
    'favorite food': 'Pasta',
    mom: {
       firstNmae: 'Rebecca',
       lastName: 'von Furstenburg',
       favoriteFood: 'Souvlaki'
    },

    dad: {
       firstName: 'Garreth',
       lastName: 'von Furstenburg',
       favoriteFood: 'Sushi'
    }
};

// console.log dad's firstName, mom's favorite food
console.log("Dad's first name: ", me.dad.firstName);
console.log("Mom's favorite food: ", me.mom.favoriteFood);


/* 
 * Tic-Tac-Toe
 * Create an array to represent the tic-tac-toe board (see slide)
 */
let  row1 = ['-', '0', '-'];
let  row2 = ['-', 'x', '0'];
let  row3 = ['x', '-', 'x'];


// After the array is created, 'O' takes the top right square.  Update.
let board = [row1, row2, row3];
board[0][2] = '0';


// Log the grid to the console.
// Hint: log each row separately.
console.log(board[0].join(','));
console.log(board[1].join(','));
console.log(board[2].join(','));


/*
 * Validate the email
 * You are given an email as string myEmail
 * make sure it is in correct email format.
 * Should be in this format, no whitespace:
 * 1 or more characters
 * @ sign
 * 1 or more characters
 * .
 * 1 or more characters
 */

const myEmail = 'foo@bar.baz'; // good email
const badEmail = 'badmail@gmail'; // bad email

console.log(/^\w+@\w+\.\w+/.test(myEmail));
console.log(/^\w+@\w+\.\w+/.test(badEmail)); 