/*const SpaceShip = function SpaceShip(spaceShipName, shipTopSpeed) {
    let name = spaceShipName;
    const topSpeed = shipTopSpeed;
    this.accelerate = function() {
        console.log(`${name} moving to ${topSpeed}`);
    };
};
*/

//The code above is commented out as to add a method that changes the topSpeed

// Keep both name and topSpeed private, but 
// add a method that changes the topSpeed
const SpaceShip = function SpaceShip(spaceShipName, shipTopSpeed) {
    const name = spaceShipName;
    let topSpeed = shipTopSpeed;
    this.accelerate = function() {
        console.log(`${name} moving to ${topSpeed}`);
    };
    this.changeTopSpeed = function(newTopSpeed) {topSpeed = newTopSpeed;}
};


// Call the constructor with a couple ships, change the topSpeed
// using the method, and call accelerate.
const shipOne = new SpaceShip('Ship One', 1000);
console.log(shipOne.name, shipOne.topSpeed);
const shipTwo = new SpaceShip('Ship Two', 100);
const shipThree = new SpaceShip('Ship Three', 2000);
const shipFour = new SpaceShip('Ship Four', 3000);

shipOne.accelerate();
shipOne.changeTopSpeed(2000);
shipOne.accelerate();

shipTwo.accelerate();
shipTwo.changeTopSpeed(25);
shipTwo.accelerate();

shipThree.accelerate();
shipThree.changeTopSpeed(1005000);
shipThree.accelerate();

shipFour.accelerate();
shipFour.changeTopSpeed(800);
shipFour.accelerate();